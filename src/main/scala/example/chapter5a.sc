import cats.data.EitherT
import cats.Monad
import cats.instances.future._
import cats.syntax.applicative._
import cats.syntax.either._
import cats.syntax.functor._
import cats.syntax.flatMap._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

type Response[A] = EitherT[Future, String, A]

12.pure[Response]

def getPowerLevel(autobot: String): Response[Int] = {
  val powerLevels = Map(
    "Jazz" -> 6,
    "Bumblebee" -> 8,
    "Hot Rod" -> 10
  )
  EitherT(Future.successful(Either.fromOption(powerLevels.get(autobot), s"$autobot is unreachable")))
}

Await.result(getPowerLevel("Jazz").map(_ + 1).value, 5.seconds)
Await.result(getPowerLevel("Jazz2").map(_ + 1).value, 5.seconds)

val powerSum = for {
  jazzPower <- getPowerLevel("Jazz")
  hodrodPower <- getPowerLevel("Hot Rod")
} yield jazzPower + hodrodPower

Await.result(powerSum.value, 1.seconds)

def canSpecialMove(ally1: String, ally2: String): Response[Boolean] =
  for {
    ally1Power <- getPowerLevel(ally1)
    ally2Power <- getPowerLevel(ally2)
  } yield ally1Power + ally2Power > 15

Await.result(canSpecialMove("Jazz", "Hot Rod").value, 1.second)
Await.result(canSpecialMove("Jazz", "Bumblebee").value, 1.second)
Await.result(canSpecialMove("Jazz", "stranger").value, 1.second)


def tacticalReport(ally1: String, ally2: String): String = {
  val report:Future[String] = canSpecialMove(ally1, ally2).value.map {
    case Right(true) => s"$ally1 and $ally2 are ready to roll out!"
    case Right(false) => s"$ally1 and $ally2 need a recharge."
    case Left(msg) => s"Comms error: $msg"
  }
  Await.result(report, 1.second)
}

tacticalReport("Jazz", "Bumblebee")
tacticalReport("Bumblebee", "Hot Rod")
tacticalReport("Jazz", "Ironhide")









