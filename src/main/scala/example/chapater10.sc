import cats.data.Validated
import cats.data.Validated.Invalid
import cats.instances.list._
import cats.kernel.Semigroup
import cats.syntax.validated._
import cats.syntax.cartesian._
import cats.syntax.semigroup._

trait Check[E, A] {
  def apply(value: A): Validated[E, A]

  def and(that: Check[E, A])(implicit semigroup: Semigroup[E]): Check[E, A] = {
    (value: A) => (this(value) |@| that(value)).map((_,_) => value)
  }

  def or(that: Check[E, A])(implicit semigroup: Semigroup[E]): Check[E, A] = {
    (value: A) => (this(value), that(value)) match {
      case (Invalid(left), Invalid(right)) => (left |+| right).invalid
      case _ => value.valid
    }
  }
}

object Check {
  def apply[E, A](func: A => Validated[E, A]) = new Check[E, A] {
    override def apply(value: A): Validated[E, A] = func(value)
  }
}

type Error = List[String]

val numberCheck = Check[Error, Int](value => value.valid[Error].ensure(List("Not a number"))(_ % 3 == 0))
val passwordCheck = Check[Error, Int](value => value.valid[Error].ensure(List("Password error"))(_ % 2 == 0))

val check = numberCheck and passwordCheck
check(6)
check(3)
check(2)
check(1)

val check2 = numberCheck or passwordCheck
check2(6)
check2(3)
check2(2)
check2(1)

