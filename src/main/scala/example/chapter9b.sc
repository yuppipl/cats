import cats.Monoid
import cats.Foldable
import cats.instances.int._
import cats.instances.string._
import cats.instances.vector._
import cats.instances.future._
import cats.syntax.foldable._
import cats.syntax.traverse._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

import scala.languageFeature.higherKinds

Foldable[Vector].foldMap(Vector(1, 2, 3))(_ + 1)
Foldable[Vector].foldMap(Vector(1, 2, 3))(identity)
Foldable[Vector].foldMap(Vector(1, 2, 3))(_.toString + "! ")
Foldable[Vector].foldMap("Hello world!".toVector)(_.toString.toUpperCase)

type Batch[A] = Vector[A]
def parallelFoldMap[A, B : Monoid](values: Batch[A])(func: A => B) : Future[B] = {
  val processors = Runtime.getRuntime.availableProcessors()
  val groupSize = (values.size.toDouble / processors).ceil.toInt
  values
    .grouped(groupSize).toVector
    .traverse(batch => Future(batch.foldMap(func)))
    .map(_.combineAll)
}

Await.result(parallelFoldMap(Vector(1, 2, 3))(_ + 1), 5 seconds)
Await.result(parallelFoldMap(Vector(1, 2, 3))(identity), 5 seconds)
Await.result(parallelFoldMap(Vector(1, 2, 3))(_.toString + "! "), 5 seconds)
Await.result(parallelFoldMap("Hello world!".toVector)(_.toString.toUpperCase), 5 seconds)
Await.result(parallelFoldMap((1 to 100000).toVector)(_ + 1), 5 seconds)
