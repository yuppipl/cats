import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import cats.data.{Writer, WriterT}
import cats.syntax.applicative._
import cats.syntax.writer._
import cats.instances.vector._

type Logged[A] = Writer[Vector[String], A]

def slowly[A](body: => A) =
  try body finally Thread.sleep(100)

def factorial(n: Int): Logged[Int] = {
  for {
    value <- slowly(if (n == 0) 1.pure[Logged] else factorial(n - 1).map(_ * n))
    _ <- Vector(s"fact $n $value").tell
  } yield value
  // val ans: Logged[Int] = slowly(if(n==0) 1.pure[Logged] else factorial(n-1).map(_* n))
  // ans.flatMap(res => res.writer(Vector(s"fact $n $res")))
}

Await.result(Future.sequence(Vector(
  Future(factorial(3).run),
  Future(factorial(4).run)
)), 5.seconds)


