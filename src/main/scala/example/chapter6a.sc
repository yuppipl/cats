import scala.language.higherKinds
import cats.Monad
import cats.instances.option._
import cats.instances.either._
import cats.instances.list._
import cats.instances.future._
import cats.syntax.functor._
import cats.syntax.flatMap._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

def product[M[_] : Monad, A, B](
                                 fa: M[A],
                                 fb: M[B]
                               ): M[(A, B)] =
  for {
    a <- fa
    b <- fb
  } yield (a, b)


product[Option, Int, String](Option(1), Some("a"))
product(Option(1), Some("a"))

type EitherOr[A] = Either[Vector[String], A]
product[EitherOr, Int, Int](Left(Vector("Error 1")), Left(Vector("Error 2")))

product(List(1,2), List(3,4))

Await.result(product(Future.successful(1), Future.successful(2)), 1.second)

