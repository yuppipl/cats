import cats.Functor
import cats.syntax.functor._

sealed trait Tree[+A]
final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
final case class Leaf[A](value: A) extends Tree[A]

implicit val treeFunctor = new Functor[Tree] {
  override def map[A, B](fa: Tree[A])(f: (A) => B): Tree[B] = fa match {
    case Branch(left: Tree[A], right: Tree[A]) => Branch(map(left)(f), map(right)(f))
    case Leaf(value: A) => Leaf(f(value))
  }
}

def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)
def leaf[A](value: A): Tree[A] = Leaf(value)

val tree = branch(leaf(1), branch(leaf(2), leaf(3)))

Functor[Tree].map(tree)(_ + 1)
Functor[Tree].map(Leaf(1))(_.toString)

tree.map(_ * 2)
leaf(1).map(_.toString)


