List(1, 2, 3).foldLeft(List[Int]())((acc, el) => el :: acc)
List(1, 2, 3).foldRight(List[Int]())(_ :: _)

def map[A, B](list: List[A])(f: A => B): List[B] =
  list.foldRight(List[B]())(f(_) :: _)

map(List(1, 2, 3))(_.toString + "!")


def flatMap[A, B](list: List[A])(f: A => List[B]): List[B] =
  list.foldRight(List[B]())(f(_) ++ _)

map(List(1, 2, 3))(x => List(x.toString, s"$x!"))

def filter[A](list: List[A])(p: A => Boolean): List[A] =
  list.foldRight(List[A]())((el, acc) => (if (p(el)) List(el) else List()) ++ acc)

filter(List(1, 2, 3))(x => x % 2 == 1)


