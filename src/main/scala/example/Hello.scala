package example

import cats.Show

object Hello extends App {
  implicit val numShow = Show.show[Int](x => x.toString)
  println(numShow.show(2))
  println("Hello1")
}
