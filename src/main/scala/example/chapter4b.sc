trait Monad[F[_]]{
  def pure[A](a: A): F[A]
  def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]
  def map[A, B](value: F[A])(func: A => B): F[B] = flatMap(value)(x => pure(func(x)))
}

import cats.Id

val idMonad = new Monad[Id] {
  override def pure[A](a: A): Id[A] = a

  override def flatMap[A, B](value: Id[A])(func: (A) => Id[B]): Id[B] = func(value)
}

idMonad.map(5)(_*2)
idMonad.flatMap(5)(_*3)
idMonad.pure("aa")



