trait Printable[A] {
  def format(value: A): String
  def contramap[B](func: B => A): Printable[B] = new Printable[B] {
    override def format(value: B): String =  Printable.this.format(func(value))
  }
}

def format[A](value: A)(implicit p: Printable[A]) = p.format(value)

implicit val stringPrintable = new Printable[String] {
  override def format(value: String): String = s"[$value]"
}

implicit val intPrintable = stringPrintable.contramap[Int](_.toString)

implicit val booleanPrintable = new Printable[Boolean] {
  override def format(value: Boolean): String = if(value) "yes" else "no"
}

format("text")
format(12)
format(true)

final case class Box[A](value: A)

implicit def boxPrintable[A](implicit a: Printable[A]) = a.contramap[Box[A]](_.value)

format(Box(true))
format(Box(11))
format(Box("Text"))
//format(Box(1.1))



