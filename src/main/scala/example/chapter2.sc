import cats.Monoid
import cats.instances.int._
import cats.instances.double._
import cats.instances.option._
import cats.syntax.semigroup._
import cats.syntax.option._


case class Order(totalCost: Double, quantity: Double)

implicit def orderMonoid(implicit doubleMonoid: Monoid[Double]) = new Monoid[Order] {
  override def empty: Order = Order(0.0, 0)

  override def combine(x: Order, y: Order): Order = Order(x.totalCost |+| y.totalCost, x.quantity |+| y.quantity)
}

def add[E: Monoid](items: List[E]) = {
  items.foldRight(Monoid[E].empty)(_ |+| _)
}

add(List(1, 2, 3, 4))
add(List(1.some, None, 2.some, 3.some, None))
add(List(Order(1.0, 2), Order(2.5, 3)))