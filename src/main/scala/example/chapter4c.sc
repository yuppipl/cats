import cats.Eval

val ans = for {
  a <- Eval.now {println("Calculating A"); 40}
  b <- Eval.now {println("Calculating B"); 2}
} yield {
  println("Adding A and B")
  a + b
}

ans.value
ans.value

def foldRight[A, B](as: List[A], acc: B)(fn: (A, B) => B): Eval[B] =
  as match {
    case head :: tail =>
      Eval.defer(foldRight(tail, acc)(fn).map(b => fn(head, b)))
    case Nil =>
      Eval.now(acc)
  }

val range : List[Int] = 1.to(50000).toList

foldRight(range, ""){
  (el, acc) => acc + el.toString
}//.value

foldRight((1 to 100000).toList, 0)(_ + _)//.value

