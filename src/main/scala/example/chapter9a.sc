import cats.Monoid
import cats.instances.int._
import cats.instances.string._
import cats.syntax.semigroup._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

def foldMap[A, B: Monoid](seq: Vector[A])(func: A => B): B =
  seq.map(func).fold(Monoid[B].empty)(_ |+| _)

foldMap(Vector(1, 2, 3))(_ + 1)
foldMap(Vector(1, 2, 3))(identity)
foldMap(Vector(1, 2, 3))(_.toString + "! ")
foldMap("Hello world!".toVector)(_.toString.toUpperCase)


val processors = Runtime.getRuntime.availableProcessors()
val values = 1 to 10
values.grouped(scala.math.ceil(values.size.toDouble / processors).toInt)




def parallelFoldMap[A, B : Monoid](values: Vector[A])(func: A => B) : Future[B] = {
  val processors = Runtime.getRuntime.availableProcessors()
  val groupSize = (values.size.toDouble / processors).ceil.toInt
  val batches: Vector[Vector[A]] = values.grouped(groupSize).toVector
  val mappedBatch: Future[Vector[B]] = Future.sequence(batches.map(batch => Future(foldMap(batch)(func))))
  mappedBatch.map(foldMap(_)(identity))
}

Await.result(parallelFoldMap(Vector(1, 2, 3))(_ + 1), 5 seconds)
Await.result(parallelFoldMap(Vector(1, 2, 3))(identity), 5 seconds)
Await.result(parallelFoldMap(Vector(1, 2, 3))(_.toString + "! "), 5 seconds)
Await.result(parallelFoldMap("Hello world!".toVector)(_.toString.toUpperCase), 5 seconds)
Await.result(parallelFoldMap((1 to 100000).toVector)(_ + 1), 5 seconds)
