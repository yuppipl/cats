import cats.data.{NonEmptyList, Validated}
import cats.data.Validated.{Invalid, Valid}
import cats.data.Kleisli
import cats.Semigroup
import cats.instances.either._
import cats.syntax.validated._
import cats.syntax.cartesian._
import cats.syntax.semigroup._

sealed trait Predicate[E, A] {

  import Predicate._

  def and(that: Predicate[E, A]) =
    And(this, that)

  def or(that: Predicate[E, A]) =
    Or(this, that)

  def apply(a: A)(implicit s: Semigroup[E]): Validated[E, A] =
    this match {
      case Pure(func) =>
        func(a)
      case And(left, right) =>
        (left(a) |@| right(a)).map((_, _) => a)
      case Or(left, right) =>
        left(a) match {
          case Valid(a1) => a.valid
          case Invalid(e1) => right(a) match {
            case Valid(a2) => a.valid
            case Invalid(e2) => (e1 |+| e2).invalid
          }
        }
    }

  def run(implicit s: Semigroup[E]): A => Either[E, A] =
    (a: A) => apply(a).toEither

}

object Predicate {

  final case class Pure[E, A](func: A => Validated[E, A]) extends Predicate[E, A]

  final case class And[E, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A]

  final case class Or[E, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A]

  def apply[E, A](func: A => Validated[E, A]): Predicate[E, A] = new Pure[E, A](func)

  def lift[E, A](error: E, func: A => Boolean): Predicate[E, A] = apply(a => a.valid.ensure(error)(func))
}

type Errors = NonEmptyList[String]

object Predicates {

  def error(s: String) = NonEmptyList(s, Nil)

  def longerThan(n: Int) = Predicate.lift[Errors, String](
    error(s"Must be longer than $n character"),
    _.length > n
  )

  def alphanumeric = Predicate.lift[Errors, String](
    error("Must be all alphanumeric characters"),
    _.forall(_.isLetterOrDigit)
  )

  def contains(char: Char) = Predicate.lift[Errors, String](
    error(s"Must contains the character $char"),
    _.contains(char)
  )

  def containsOnce(char: Char) = Predicate.lift[Errors, String](
    error(s"Must contains the character $char only once"),
    _.count(_ == char) == 1
  )

  def tuple[E: Semigroup, A, B](predicateLeft: Predicate[E, A], predicateRight: Predicate[E, B]) = Predicate[E, (A, B)] {
    case (l, r) =>
      (predicateLeft(l) |@| predicateRight(r)).tupled
  }
}

import Predicates._

type Result[A] = Either[Errors, A]

type Check[A, B] = Kleisli[Result, A, B]

def check[A](predicate: Predicate[Errors, A]) : Check[A, A] = Kleisli[Result, A, A] (predicate.run)

val checkUsername = check(longerThan(4) and alphanumeric)

val checkEmail = Kleisli[Result, String, String] {
  val checkName = longerThan(0)
  val checkDomain = longerThan(3) and contains('.')

  check(containsOnce('@'))
    .map(_.split('@') match {
      case Array(name, domain) => (name, domain)
    })
    .andThen(tuple(checkName, checkDomain).run)
    .map { case (l, r) => s"$l@$r" }.run
}

final case class User(name: String, email: String)

def createUser(name: String, email: String): Result[User] =
  (checkUsername.run(name) |@| checkEmail.run(email)).map(User)

createUser("Pawel", "yuppipl@gmail.com")
createUser("Daniel", "daniel")