import cats.Monad
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.applicative._

sealed trait Tree[+A]

final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

final case class Leaf[A](value: A) extends Tree[A]

def branch[A](left: Tree[A], right: Tree[A]): Tree[A] =
  Branch(left, right)

def leaf[A](value: A): Tree[A] =
  Leaf(value)

implicit val treeMonad = new Monad[Tree] {
  override def pure[A](x: A): Tree[A] = leaf(x)

  override def flatMap[A, B](fa: Tree[A])(f: (A) => Tree[B]): Tree[B] = fa match {
    case Leaf(value) => f(value)
    case Branch(left, right) => branch(flatMap(left)(f), flatMap(right)(f))
  }

  override def tailRecM[A, B](a: A)(f: (A) => Tree[Either[A, B]]): Tree[B] = f(a) match {
    case Leaf(Left(a1)) => tailRecM(a1)(f)
    case Leaf(Right(b)) => Leaf(b)
    case Branch(left, right) => branch(
      flatMap(left) {
        case Left(l1) => tailRecM(l1)(f)
        case Right(r1) => Leaf(r1)
      }, flatMap(right) {
        case Left(l2) => tailRecM(l2)(f)
        case Right(r2) => Leaf(r2)
      })
  }

}

treeMonad.pure(5)
treeMonad.flatMap(leaf(1))(value => branch(leaf(value * 10), leaf(value * 100)))
treeMonad.flatMap(branch(leaf(1), leaf(2)))(value => branch(leaf(value * 10), leaf(value * 100)))
treeMonad.flatMap(branch(leaf(3), branch(leaf(1), leaf(2))))(value => branch(leaf(value * 10), leaf(value * 100)))
treeMonad.map(leaf(1))(_ * 10)
treeMonad.map(branch(leaf(1), leaf(2)))(_ * 10)

5.pure[Tree]
leaf(1).flatMap(value => branch(leaf(value * 10), leaf(value * 100)))
branch(leaf(1), leaf(2)).flatMap(value => branch(leaf(value * 10), leaf(value * 100)))
leaf(1).map(_ * 10)
branch(leaf(1), leaf(2)).map(_ * 10)

for {
  value <- branch(leaf(1), leaf(2))
} yield branch(leaf(value * 10), leaf(value * 100))


