import scala.language.higherKinds
import cats.{Applicative, Functor, Id}
import cats.syntax.traverse._
import cats.syntax.functor._
import cats.instances.list._


import scala.concurrent.Future

trait UptimeClient[F[_]] {
  def getUptime(hostname: String): F[Int]
}

trait RealUptimeClient extends UptimeClient[Future] {
  def getUptime(hostname: String): Future[Int]
}

class TestUptimeClient(hosts: Map[String, Int]) extends UptimeClient[Id] {
  override def getUptime(hostname: String): Int =
    hosts.getOrElse(hostname, 0)
}

class UptimeService[F[_] : Applicative](client: UptimeClient[F]){
  def getTotalUptime(hostnames: List[String]): F[Int] =
    hostnames.traverse(client.getUptime).map(_.sum)
}

val hosts = Map("host1" -> 10, "host2" -> 6)
val client = new TestUptimeClient(hosts)
val service = new UptimeService(client)
var actual = service.getTotalUptime(hosts.keys.toList)
var expected = hosts.values.sum
actual == expected

