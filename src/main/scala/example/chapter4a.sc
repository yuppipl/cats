trait Monad[F[_]]{
  def pure[A](a: A): F[A]
  def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]
  def map[A, B](value: F[A])(func: A => B): F[B] = flatMap(value)(x => pure(func(x)))
}

val optionMonad = new Monad[Option] {
  override def pure[A](a: A): Option[A] = Option(a)

  override def flatMap[A, B](value: Option[A])(func: (A) => Option[B]): Option[B] = value.flatMap(func)
}

optionMonad.map(Option(1))(x => x.toString)
