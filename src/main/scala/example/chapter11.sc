import cats.Semigroup
import cats.instances.int._
import cats.instances.string._
import cats.instances.map._
import cats.syntax.semigroup._
import cats.Monoid

trait BoundedSemiLattice[A] extends Monoid[A] {
  def combine(x: A, y: A): A

  def empty: A
}



object BoundedSemiLattice {
  implicit val intBoundedSemiLattice = new BoundedSemiLattice[Int] {
    override def combine(x: Int, y: Int): Int = x max y

    override def empty: Int = 0
  }
}

implicit val mapSemigroup = new Semigroup[Map[String, Int]] {
  override def combine(x: Map[String, Int], y: Map[String, Int]): Map[String, Int] = {
    val keys = x.keySet ++ y.keySet
    keys.map(key => (key, x.getOrElse(key, 0) max y.getOrElse(key, 0))).toMap
  }
}

final case class GCounter(counters: Map[String, Int]) {
  def increment(machine: String, amount: Int): GCounter =
    GCounter(counters + (machine -> (amount + counters.getOrElse(machine, 0))))

  def get: Int =
    counters.values.sum

  def merge(that: GCounter): GCounter =
    GCounter(mapSemigroup.combine(this.counters, that.counters))
}

val counterA = GCounter(Map("a" -> 2))
val counterB = GCounter(Map("a" -> 1, "b" -> 2))
val counterA2 = counterA.increment("a", 1)
val counterMerged = counterA2.merge(counterB)
counterMerged.get

