import cats.data.State

import scala.util.Try

type CalcState[A] = State[List[Int], A]

def evalOne(sym: String): CalcState[Int] = {

  lazy val parseNumber: Option[CalcState[Int]] = Try(sym.toInt).toOption.map(num => {
    State[List[Int], Int] { oldStack: List[Int] =>
      (num :: oldStack, num)
    }
  })

  lazy val parseOperator: Option[CalcState[Int]] = {
    val operator: PartialFunction[String, (Int, Int) => Int] = {
      case "+" => _ + _
      case "*" => _ * _
    }
    operator.lift(sym).map(operatorFunction => {
      State[List[Int], Int] { oldStack: List[Int] =>
        val newStack = oldStack match {
          case op1 :: op2 :: t => Some(operatorFunction(op1, op2) :: t)
          case _ => None
        }
        newStack.map(stack => (stack, stack.head)).getOrElse(oldStack, oldStack.head)
      }
    })
  }

  def firstSome[A](elements: Seq[Option[A]]): Option[A] = elements.fold(None)((acc, el) => acc match {
    case Some(_) => acc
    case None => el
  })

  firstSome(List(parseNumber, parseOperator)).get
}

def evalAll(input: List[String]): CalcState[Int] =
  input.foldLeft(State.pure[List[Int], Int](0))((acc, sym) => acc.flatMap(_ => evalOne(sym)))

val program = for {
  _ <- evalOne("1")
  _ <- evalOne("2")
  _ <- evalOne("3")
  ans <- evalOne("*")
  ans2 <- evalOne("+")
} yield ans2

program.run(Nil).value

val program2 = evalAll(List("1", "2", "+", "3", "*"))

program2.run(Nil).value

val program3 = for {
  _ <- evalAll(List("1", "2", "+"))
  _ <- evalAll(List("3", "4", "+"))
  ans <- evalOne("*")
} yield ans

program3.run(Nil).value
