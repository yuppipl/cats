import cats.Traverse
import cats.instances.list._
import cats.instances.vector._

Traverse[List].sequence(List(Vector(1, 2), Vector(3, 4)))
Traverse[List].sequence(List(Vector(1, 2), Vector(3, 4), Vector(5, 6)))

import cats.instances.option._

def process(inputs: List[Int]) =
  Traverse[List].traverse[Option, Int, Int](inputs)(n => if (n % 2 == 0) Some(n) else None)

process(List(2, 4, 6))
process(List(1, 2, 3))

import cats.data.Validated

type Error = List[String]
type ErrorOr[A] = Validated[Error, A]

def processValidated(inputs: List[Int]): ErrorOr[List[Int]] =
  Traverse[List].traverse[ErrorOr, Int, Int](inputs) { n =>
    if (n % 2 == 0) {
      Validated.valid[Error, Int](n)
    } else {
      Validated.invalid[Error, Int](List(s"$n is not even"))
    }
  }

processValidated(List(2, 4, 6))
processValidated(List(1, 2, 3))