import cats.data.Validated

import scala.util.Try
import cats.instances.either._
import cats.instances.list._
import cats.instances.unit._

import cats.syntax.either._
import cats.syntax.cartesian._
import cats.syntax.semigroup._
import cats.syntax.flatMap._
import cats.syntax.functor._

type FormData = Map[String, String]
type Error = List[String]
type ErrorOr[A] = Either[Error, A]
type AllErrorOr[A] = Validated[Error, A]

case class User(name: String, age: Int)

def getValue(name: String)(dataForm: FormData): ErrorOr[String] = dataForm.get(name).toRight(List(s"$name field not specified"))
val getName = getValue("name") _
val getAge = getValue("age") _

def parseInt(input: String): ErrorOr[Int] = Try(input.toInt).toEither.leftMap(_ => List(s"$input must be an integer"))

def nonBlank(value: String): ErrorOr[String] = Either.right[Error, String](value).ensure(List(s"$value cannot be blank"))(_.nonEmpty)
def nonNegative(value: Int): ErrorOr[Int] = Either.right[Error, Int](value).ensure(List(s"$value must b non-negative"))(_ >= 0 )

def readName(dataForm: FormData): ErrorOr[String] = for {
  name <- getName(dataForm)
  _ <- nonBlank(name)
} yield name

def readAge(dataForm: FormData): ErrorOr[Int] = for {
  ageInText <- getAge(dataForm)
  _ <- nonBlank(ageInText)
  age <- parseInt(ageInText)
  _ <- nonNegative(age)
} yield age

val form = Map("name" -> "Pawel", "age" -> "10")
val userValidated: AllErrorOr[User] = (readName(form).toValidated |@| readAge(form).toValidated).map(User.apply)

val form2 = Map("name2" -> "Pawel", "age" -> "-5")
val userValidated2: AllErrorOr[User] = (readName(form2).toValidated |@| readAge(form2).toValidated).map(User.apply)



